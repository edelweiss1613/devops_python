import socket
import ipaddress

class Router:
    def __init__(self):
        self.ip_list = []
        self.route_list = []

    # IP FUNCTIONS
    def add_ip(self, ip, brd, mask, iface):
        if Router.check_ip(ip) and Router.check_ip(brd) and Router.check_ip(mask):
            self.ip_list.append([ip, brd, mask, iface])

    def del_ip(self, id):
        try:
            del self.ip_list[id]
        except IndexError:
            print('ERROR: IP with ID == {} not found'.format(id))

    def show_ips(self):
        print("\n{:<5}{:<20}{:<20}{:<20}{:<15}".format('ID', 'IP', 'Broadcast', 'Mask', 'Interface'))
        print('-' * 80)
        c = 0
        for ip in self.ip_list:
            print("{:<5}{:<20}{:<20}{:<20}{:<15}".format(c, ip[0], ip[1], ip[2], ip[3]))
            c += 1
        print('-' * 80)

    # ROUTES FUNCTIONS
    def add_route(self, dest, gateway, genmask, iface):
        if Router.check_ip(dest) and Router.check_ip(gateway) and Router.check_ip(genmask):
            self.route_list.append([dest, gateway, genmask, iface])

    def del_route(self, id):
        try:
            del self.route_list[id]
        except IndexError:
            print('ERROR: Route with ID == {} not found'.format(id))

    def show_routes(self):
        print("\n{:<5}{:<20}{:<20}{:<20}{:<15}".format('ID', 'Destination', 'Gateway', 'Genmask', 'Interface'))
        print('-' * 80)
        c = 0
        for route in self.route_list:
            print("{:<5}{:<20}{:<20}{:<20}{:<15}".format(c, route[0], route[1], route[2], route[3]))
            c += 1
        print('-' * 80)

    @staticmethod
    def check_ip(ip):
        res = False
        try:
            socket.inet_aton(ip)
            res = True
        except OSError:
            print('ERROR: Illegal IP address {} passed to inet_aton'.format(ip))
        return res


router = Router()
router.add_ip('192.168.88.26', '192.168.88.255', '255.255.255.0', 'wlan6')
router.add_ip('192.168.88.2', '192.168.88.255', '255.255.255.0', 'wlan6')
router.add_ip('192.168.88.35', '192.168.88.255', '255.255.255.0', 'virbr0')
router.add_ip('192.168.88.155', '192.168.88.255', '255.255.255.0', 'virbr0')
router.del_ip(3)

router.add_route('10.0.0.0', '192.168.88.1', '0.0.0.0', 'wlan6')
router.add_route('192.168.88.0', '0.0.0.0', '255.255.255.0', 'wlan6')
router.add_route('192.168.122.0', '0.0.0.0', '255.255.255.0', 'virbr0')
router.add_route('192.168.122.155', '0.0.0.0', '255.255.255.0', 'virbr0')
router.del_route(3)

router.show_ips()
router.show_routes()

