from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread
import datetime
import random
import os
import threading


def accept_incoming_connections():
    """Sets up handling for incoming clients."""
    while True:
        client, client_address = SERVER.accept()
        print("%s:%s has connected." % client_address)
        client.send("Hi User! Now type your name and press enter!".encode('utf-8'))
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()


def handle_client(client):
    """Handles a single client connection."""

    name = client.recv(BUFSIZ).decode("utf8").rstrip('\r\n')

    if name == ':q':
        #Kill current thread
        return False

    if is_name_free(name):
        if not is_correct_name(name):
            info = "!!! Incorrect name. Name mustn't started by ':'".format(name)
            repeat_request(info, client)

    else:
        info = '!!! This name already exists. Please choose new name and press enter!'.format(name)
        repeat_request(info, client)

    #Send welcome message
    welcome = '<<< Welcome {} If you ever want to quit, type :q to exit. >>>\n' \
              '<<< If you want to send the private message, type USERNAME: [message] ' \
              'For example - Alice: Hi! How are you >>>'.format(name)
    client.send(welcome.encode('utf-8'))
    msg = "--- %s has joined the chat! ---" % name

    try:
        broadcast(msg)
        clients[client] = name
        user_color[name] = add_user_color()
    except:
        pass

    while True:
        try:
            msg = client.recv(BUFSIZ).decode('utf-8').rstrip('\r\n')
            if check_msg(msg, name, client):
                pass
            else:
                #Kill current thread
                return False

        except:
            pass


def is_name_free(name):
    for user_sock, user_name in clients.items():
        if user_name == name:
            # name += '_2'
            return False
    return True


def is_correct_name(name):
    if name[0] == ':':
        return False
    return True


def repeat_request(info, client):
    try:
        client.send(info.encode('utf-8'))
        handle_client(client)
        return True
    except:
        return False


def add_user_color():
    """Random add color to user"""
    return random.choice(colors)


def check_msg(msg, name, client):
    """Check message for command"""

    #Quit command
    if msg == ":q":
        client.send(":q".encode('utf-8'))
        client.close()
        del clients[client]
        print("--- {} has left the chat ---".format(name))
        broadcast("--- {} has left the chat ---".format(name))
        return False


    #Show all users command
    elif msg == ':show':
        users = [name for sock, name in clients.items()]
        res = '\n--- USER LIST ---\n'
        i = 0
        end = '\n'
        for u in users:
            i += 1
            if i == len(users):
                end = '\n--- END USER LIST ---'
            res += str(u) + end
        client.send(bytes(res, 'utf-8'))
        return True

    #Private message
    elif msg[0] == ':' and split_msg(msg):
        recipient = split_msg(msg)
        if del_recipient_from_message(msg, recipient):
            msg = del_recipient_from_message(msg, recipient)
            if private(msg, name, recipient):
                pass
        return True

    #Public message
    else:
        broadcast(msg, name+": ", user_color[name])
        return True
    return False


def private(msg, prefix="", recipient=""):
    """Handling the private message
    Private message are always red
    """
    msg_time = datetime.datetime.now().strftime('%H:%M:%S')
    send_from = ''
    send_to = ''
    for user_sock, user_name in clients.items():

        if user_name == prefix:
            send_from = user_sock
        if user_name == recipient:
            send_to = user_sock

    if send_from and send_to:

        send_from.send(bytes('\033[1;31;40m', 'utf-8') +
                       bytes('[{}] '.format(msg_time), 'utf-8') +
                       bytes('--- You to ', 'utf-8') +
                       bytes(recipient, 'utf-8') +
                       bytes(':', 'utf-8') +
                       bytes(msg, 'utf-8') +
                       bytes('\033[0m', 'utf-8'))

        if send_from != send_to:
            send_to.send(bytes('\033[1;31;40m', 'utf-8') +
                         bytes('[{}] '.format(msg_time), 'utf-8') +
                         bytes(prefix, 'utf-8') +
                         bytes(' for you:', 'utf-8') +
                         bytes(msg, 'utf-8') +
                         bytes('\033[0m', 'utf-8'))
    return True


def split_msg(msg):
    """Split message"""
    try:
        return msg.split()[0].split(':')[1]
    except:
        pass
    return False


def del_recipient_from_message(msg, recipient):
    """Remove the recipient name from message"""
    try:
        len_rec = len(recipient) + 1
        return msg[len_rec:]
    except:
        pass
    return False


def broadcast(msg, name="", color="\033[1;37;40m"):
    """Broadcasts a message to all the clients."""
    msg_time = datetime.datetime.now().strftime('%H:%M:%S')
    for sock in clients:
        sock.send(bytes(color, 'utf-8') +
                  bytes('[{}] '.format(msg_time), 'utf-8') +
                  bytes(name, 'utf-8') +
                  bytes(msg, 'utf-8') +
                  bytes('\033[0m', 'utf-8'))

        
clients = {}
addresses = {}
colors = [
    '\033[1;32;40m',
    '\033[1;33;40m',
    '\033[1;34;40m',
    '\033[1;35;40m',
    '\033[1;36;40m',
]
user_color = {}

HOST = ''
PORT = 7777
BUFSIZ = 1024
ADDR = (HOST, PORT)

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)

if __name__ == "__main__":
    SERVER.listen(5)
    print("Waiting for connection...")
    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    SERVER.close()