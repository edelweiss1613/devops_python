# Python program to implement client side of chat room. 
import socket
import select
import sys

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
IP_address = '127.0.0.1'
Port = 7777
server.connect((IP_address, Port))


while True:

    try:
        sockets_list = [sys.stdin, server]
        read_sockets, write_socket, error_socket = select.select(sockets_list, [], [])

        for socks in read_sockets:
            if socks == server:
                message = socks.recv(2048)
                if message:
                    print(message.decode('utf-8'))
            else:
                try:
                    message = sys.stdin.readline()
                    if message.rstrip('\n') == ':q':
                        raise KeyboardInterrupt
                    server.send(message.encode('utf-8'))
                    sys.stdout.flush()
                except:
                    raise KeyboardInterrupt

    except:
        server.send(':q'.encode('utf-8'))
        break
server.close()
