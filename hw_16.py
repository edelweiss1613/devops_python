import googlemaps
import os
import argparse
from hw_17 import *
import sys


def attention_default_file():

    print('----- Attention -----\n'
          'Used default image \"img.jpg\"\n'
          'If you want to use a different image, please read the documentation: '
          'python3 {} -h\n'
          '----- End-----\n'.format(sys.argv[0]))

# DEFAULT FILENAME
file_name = ''

parser = argparse.ArgumentParser()
parser.add_argument("filename", nargs='?', help="image path, default = img.jpg")

args = parser.parse_args()

if args.filename is not None:
    if os.path.isfile(args.filename):
        file_name = args.filename
    else:
        exit('File {} not found'.format(args.filename))
else:
    file_name = 'img.jpg'
    attention_default_file()


def get_info_by_coordinates(lat, lon):
    result = {}
    try:
        gmaps = googlemaps.Client(key='AIzaSyBMU6prp1esf8Ezv5FxbZUI6fzorTb_L94')
        result['location'] = gmaps.reverse_geocode((lat, lon))[0]['formatted_address']
        result['lat'] = lat
        result['lon'] = lon
    except ValueError:
        print('Cannot convert latitude and longtitude')
    return result


coordinates = get_result(file_name)

if coordinates:
    result = get_info_by_coordinates(coordinates['lat'], coordinates['lon'])
    print('Input data: {},{}'.format(coordinates['lat'], coordinates['lon']))
    print('Location: {}'.format(result['location']))
    print('Google Maps URL: https://www.google.com/maps/search/?api=1&query={},{}'.format(coordinates['lat'], coordinates['lon']))
elif not coordinates:
    print('Cannot find gps coordinates in {} '.format(file_name))