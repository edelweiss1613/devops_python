# PROBLEM 9
problem_9 = [a*b*c for a in range(1, 1000) for b in range(a+1, 1000) for c in range(1000-a-b, 1000-a-b+1) if (a ** 2 + b ** 2 == c ** 2) and (a + b + c == 1000)][0]

# PROBLEM 6
problem_6 = (sum([x for x in range(1, 101)])**2) - (sum([x**2 for x in range(1, 101)]))

# PROBLEM 48
problem_48 = str(sum([x**x for x in range(1, 1001)]))[-10:]

# PROBLEM 40
line = ''.join([str(x+1) for x in range(0, 1000000)])

'''
Есть более оптимизированнное решение, но я не смог его записать в одну строку.
'''
# for x in range(0, 1000000):
#     if len(line) <= 1000000:
#         line += str(x+1)

problem_40 = int(line[0])*int(line[9])*int(line[99])*int(line[999])*int(line[9999])*int(line[99999])*int(line[999999])


print('Result for Problem 9: ' + str(problem_9))
print('Result for Problem 6: ' + str(problem_6))
print('Result for Problem 48: ' + problem_48)
print('Result for Problem 40: ' + str(problem_40))
