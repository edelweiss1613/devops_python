def fib(n):

    a = b = 1

    result = [a, b]

    if n > 2:
        for i in range(0, n-2):
            c = a + b
            a = b
            b = c
            result.append(c)
    return result

inp = int(input('Please enter the position number: '))

output = fib(inp)
string = ', '.join(str(x) for x in output)
last = 'Element by position %s is %s \n' % (inp, output[-1])


print(last, string)
