inp = int(input('Please enter the number: '))


def find_num(i, count=0):

    if i % 2 == 0:
        i = i / 2
        return find_num(i, count+1)

    elif i > 1:
        i = i*3 + 1
        return find_num(i, count+1)

    return count


print('Count: ' + str(find_num(inp)))