def calc(n):
    if n % 2 == 0:
        return int(n / 2)
    else:
        return int(n * 3 + 1)


while True:
    inp = input("Please input the number: ")

    try:
        result = int(inp)
        print(calc(result))

    except ValueError:
        print('Не удалось преобразовать введенный текст в число.')

    if inp == 'cancel':
        print('Bye!')
        break
