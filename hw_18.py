from PIL import Image, ImageOps
import argparse
import os.path
import sys

def attention_default_file():

    print('----- Attention -----\n'
          'Used default image \"img.jpg\"\n'
          'If you want to use a different image, please read the documentation: '
          'python3 {} -h\n'
          '----- End-----\n'.format(sys.argv[0]))

filename = ''

parser = argparse.ArgumentParser()
parser.add_argument("filename", nargs='?', help="image path, default = img.jpg")
parser.add_argument("--width", type=int, default=128, help="width for thumbnail (default=128)")
parser.add_argument("--height", type=int, default=128, help="height for thumbnail (default=128)")
parser.add_argument('--crop', "-c", default='false', choices=['true', 'false'],
                    help="Whether to crop images to specified width and height or resize. "
                         "An array can specify positioning of the crop area.(default=false)")
args = parser.parse_args()


if args.filename is not None:
    if os.path.isfile(args.filename):
        filename = args.filename
    else:
        exit('File {} not found'.format(args.filename))
else:
    filename = 'img.jpg'
    attention_default_file()



size = (args.width, args.height)
img = Image.open(filename)

sep = '.'
name_without_type = filename.split(sep, 1)[0]
img_type = filename.split(sep, 1)[1]


def create_thumb(size):
    thumb_path = ''
    result = ''
    if args.crop == 'true':
        img.thumbnail(size, Image.ANTIALIAS)
        size = img.size
        thumb_path = '{}_thumb_{}x{}.{}'.format(name_without_type, size[0], size[1], img_type)
        img.save(thumb_path)

    else:
        im = ImageOps.fit(img, size, Image.ANTIALIAS)
        thumb_path = '{}_thumb_{}x{}.{}'.format(name_without_type, size[0], size[1], img_type)
        im.save(thumb_path)

    if os.path.isfile(thumb_path):
        result = '{} thumbnail created successfully'.format(thumb_path)
    return result


print(create_thumb(size))