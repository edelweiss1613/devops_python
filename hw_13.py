
def convert(temp, vector):
    if vector == '1':
        res = round(((temp - 32) * 5 / 9), 2)
        return res
    elif vector == '2':
        res = round(((temp * 9 / 5) + 32), 2)
        return res
    else:
        return 'The option does not exist'


def select_calc():

    inp = input('Select convert type:\n'
              '1 - °F to °C\n'
              '2 - °C to °F\n'
              '3 - Exit\n')

    if inp == '1':
        inp_t = input_temp()
        res = '\n--- Result from convert %s°F to °C: %s°C ---\n' % (inp_t, (convert(inp_t, inp)))
    elif inp == '2':
        inp_t = input_temp()
        res = '\n--- Result from convert %s°C to °F: %s°F ---\n' % (inp_t, (convert(inp_t, inp)))
    elif inp == '3':
        print('Bye!')
        exit()
    else:
        return 'The option does not exist'

    return res


def input_temp():
    inp_temp = input('Please input the temperature: ')

    try:
        inp_temp = float(inp_temp)
    except ValueError:
        print('Temperature must be a integer')

    return inp_temp


while True:
    print(select_calc())


