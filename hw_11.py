import string


def letters_range(start='a', stop='z', step=1):

        alphabet = string.ascii_lowercase

        start = alphabet.index(start)
        stop = alphabet.index(stop)

        range_alpabet = alphabet[start:stop:step]

        return list(range_alpabet)


print('For (b, w, 2):')
print(letters_range('b', 'w', 2), end="\n----------\n")

print('For (a, g)')
print(letters_range('a', 'g'), end="\n----------\n")

print('For (g, p)')
print(letters_range('g', 'p'), end="\n----------\n")

print('For (p, g, -2)')
print(letters_range('p', 'g', -2), end="\n----------\n")

print('For (a, a)')
print(letters_range('a', 'a'))


