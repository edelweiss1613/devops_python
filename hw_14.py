#!/usr/bin/env python3

import pickle

class Employee:
    def __init__(self, name, phone, salary=10000):
        self.name = name
        self.phone = phone
        self.salary = salary

    def print_salary_info(self):
        print("Employee {} gets {} Rubles".format(self.name, self.salary))

    def add_salary(self, delta=5000):
        self.salary = self.salary + delta

    def add(self, other_empl):
        new_name = self.name + "&" + other_empl.name
        new_phone = str(round((int(self.phone) + int(other_empl.phone)) / 2))
        new_salary = self.salary + other_empl.salary
        return Employee(new_name, new_phone, new_salary)

    __add__ = add


mary = Employee(name='Mary', phone='987654', salary=100500)

with open('mary.pickle', 'wb') as f:
    pickle.dump(mary, f)

with open('mary.pickle', 'rb') as f:
    mary_new = pickle.load(f)

print(mary_new)

'''
Преимущество использования pickle заключается в том,
что он может сериализовать практически любой объект Python, не добавляя лишнего кода.
Его прелесть также в том, что он будет записывает какой-либо один объект один раз,
что делает его эффективным для хранения рекурсивных структур, таких как графики.

Основной минус его в том, что он должен работать только с проверенными данными,
иначе последствия могут быть непредсказуемы и небезопасны.

Также проблема заключается в его скорости работы. Еще один из недостатков - это нечитаемость.

В большинстве задач лучше использовать json.

'''