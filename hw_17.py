from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS


def get_gps_data(image_source):
    image = Image.open(image_source)
    info = image._getexif()
    gps_data = {}

    try:
        for tag, value in info.items():
            decoded = TAGS.get(tag, tag)
            if decoded == "GPSInfo":
                for t in value:
                    sub_decoded = GPSTAGS.get(t, t)
                    gps_data[sub_decoded] = value[t]
    except:
        pass

    if gps_data \
            and ('GPSLatitude' in gps_data) \
            and ('GPSLatitudeRef' in gps_data) \
            and ('GPSLongitude' in gps_data) \
            and('GPSLongitudeRef' in gps_data):
        return gps_data
    else:
        return False


def convert_gps(value):
    d0 = value[0][0]
    d1 = value[0][1]
    d = float(d0) / float(d1)

    m0 = value[1][0]
    m1 = value[1][1]
    m = float(m0) / float(m1)

    s0 = value[2][0]
    s1 = value[2][1]
    s = float(s0) / float(s1)

    return d + (m / 60.0) + (s / 3600.0)


def get_gps_pos(exif_gps):

    if exif_gps:
        gps_latitude = exif_gps["GPSLatitude"]
        gps_latitude_ref = exif_gps['GPSLatitudeRef']
        gps_longitude = exif_gps['GPSLongitude']
        gps_longitude_ref = exif_gps['GPSLongitudeRef']

        lat = convert_gps(gps_latitude)
        if gps_latitude_ref != "N":
            lat = 0 - lat

        lon = convert_gps(gps_longitude)
        if gps_longitude_ref != "E":
            lon = 0 - lon

        return {'lat': lat, 'lon': lon}
    else:
        return False


def get_result(file_name):
    return get_gps_pos(get_gps_data(file_name))