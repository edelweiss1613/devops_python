inp = input("Please input text: \n").lower().split()
words = {}

for i in inp:

    if i in words:
        words[i] += 1
    else:
        words[i] = 1

res = sorted(words.items(), key=lambda k: k[1], reverse=True)

biggest = res[0][1]

for i in res:

    if biggest == i[1]:
        print(i[0] + ' - ' + str(i[1]))

