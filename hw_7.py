f = open('passwd', 'r')
line = f.readlines()

f_2 = open('group', 'r')
line_2 = f_2.readlines()

res = {}
res['shell'] = {}
res['group'] = {}
users = {}

for l in line:
    user = l.split(':')
    shell = user[6].strip('\n')
    users[user[0]] = user[2]

    if shell in res['shell']:
        res['shell'][shell] += 1
    else:
        res['shell'][shell] = 1

f.close()

for l_2 in line_2:
    line_group = l_2.split(':')
    user = line_group[3].strip('\n')
    group = line_group[0]

    if user == '':
        user = group

        if user in users:
            res['group'][group] = users[user]

        # Если необходимо в выводе убрать группы без пользователей, то можно заккоментировать код внутри комментария
        else:
            res['group'][group] = ''
        # Конец комментария

    else:
        user = user.split(',')
        uid = []

        for u in user:
            if u in users:
                uid.append(users[u])

        res['group'][group] = ','.join(uid)


f_2.close()

sorted_shell = sorted(res['shell'].items(), key=lambda kv: kv[1], reverse=True)
sorted_group = sorted(res['group'].items())

out = open('output.txt', 'w')

out.writelines("---SHELL---\n\n")

for i in sorted_shell:
    string = ' - '.join(str(x) for x in i)
    out.writelines(string + '\n')

out.writelines("\n---GROUP---\n\n")
for i in sorted_group:
    string = ':'.join(i)
    out.writelines(string + '\n')

out.close()
