res = 0
for i in range(1, 1000000):
    num_10 = str(i)
    num_2 = str(bin(i)[2:])


    if num_10 == num_10[::-1] and num_2 == num_2[::-1]:
        print(str(num_10) + ' - ' + str(num_2))
        res += int(num_10)

print('Sum: ' + str(res))
