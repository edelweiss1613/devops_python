import socket
import validators


custom_hosts = {}


def add_host(domain_name, ip):
    if validators.domain(domain_name) and validators.ipv4(ip):
        custom_hosts[domain_name] = ip
        return True
    else:
        return False


def get_ip(domain_name):
    if validators.domain(domain_name):

        if domain_name in custom_hosts:
            return custom_hosts[domain_name]
        else:
            try:
                return socket.gethostbyname(domain_name)
            except:
                return 'code_1'
    else:
        return 'code_2'


def parse_data(data):
    data_list = data.split(' ')
    if data_list[0] == 'ADD':
        try:
            host_ip = data_list[1].split(':')
            if add_host(host_ip[0], host_ip[1]):
                add_host(host_ip[0], host_ip[1])
                return host_ip
            else:
                return 'code_1'
        except:
            print('Data not valid')
    return False


def send_res(msg, server_socket, address):
    print(msg, end='')
    res = msg.encode('utf-8')
    server_socket.sendto(res, address)


def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(('localhost', 5000))
    print("UDP - DNS Server listening on port 5000:")

    while True:

        data, address = server_socket.recvfrom(512)
        data = data.decode('utf-8').replace("\n", "")
        parse = parse_data(data)

        if parse not in (False, 'code_1'):
            send_res('Domain {} with IP: {} successful added\n'.format(parse[0], parse[1]), server_socket, address)

        elif parse == 'code_1':
            send_res('Cannot add data {}\n'.format(data), server_socket, address)

        else:
            res = get_ip(data)

            if res and res not in ('code_1', 'code_2'):
                send_res('Domain {} has IP: {}\n'.format(data, res), server_socket, address)

            elif res == 'code_1':
                send_res("Unable to get IP for " + str(data) + '\n', server_socket, address)

            elif res == 'code_2':
                send_res('Domain \"{}\" is not valid\n'.format(str(data)), server_socket, address)


start_server()

