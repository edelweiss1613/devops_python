inp = input('Please enter numbers: \n').split()

min_num = int(min(inp))
max_num = int(max(inp))

res = 0

for i in range(min_num, max_num):
    if str(i) not in inp:
        res = i
        break


if res == 0:
    if min_num > 1:
        res = min_num - 1
    else:
        res = max_num + 1


print(res)
